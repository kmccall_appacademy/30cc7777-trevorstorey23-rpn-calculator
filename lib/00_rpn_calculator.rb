class RPNCalculator


  def initialize
    @stack = []
  end

  def value
    @stack.last
  end

  def push(el)
    @stack.push(el)
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def divide
    perform_operation(:/)
  end

  def times
    perform_operation(:*)
  end

  def perform_operation(operation)
    raise "calculator is empty" if @stack.empty?
    num2 = @stack.pop
    num1 = @stack.pop

    if operation == :+
      @stack << num1 + num2
    elsif operation == :-
      @stack << num1 - num2
    elsif operation == :*
      @stack << num1 * num2
    elsif operation == :/
      @stack << num1.to_f / num2.to_f
    end

    value
  end

  def tokens(string)
    string.split.map { |chr| operation?(chr) ? chr.to_sym : chr.to_i }
  end

  def operation?(chr)
    ["+", "-", "/", "*"].include?(chr)
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each {|e| e.is_a?(Symbol) ? perform_operation(e) : @stack.push(e)}
    value
  end


end
